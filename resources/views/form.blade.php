<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome">
        <label for="">First Name:</label>
        <br><br>
        <input type="text">
        <br><br>

        <label for="">Last Name:</label>
        <br><br>
        <input type="text">
        <br><br>

        <label for="">Gender:</label>
        <br><br>
        <input type="radio" name="gender">Male
        <br>
        <input type="radio" name="gender">Female
        <br>
        <input type="radio" name="gender">Other
        <br><br>

        <label for="">Nationality:</label>
        <br><br>
        <select name="national" id="">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapore">Singapore</option>
            <option value="other">Other</option>
        </select>
        <br><br>

        <label for="">Language Spoken:</label>
        <br><br>
        <input type="checkbox" name="indo" id="">Bahasa Indonesia
        <br>
        <input type="checkbox" name="english" id="">English
        <br>
        <input type="checkbox" name="other" id="">Other
        <br><br>

        <label for="">Bio:</label>
        <br><br>
        <textarea name="" id="" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up" >
    </form>
</body>
</html>